package id.ac.uinjkt.eprogress.config;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.session.HttpSessionEventPublisher;

/**
 *
 * @author Pustipanda
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    private final AccessDeniedHandler accessDeniedHandler;

    @Autowired
    private SessionRegistry sessionRegistry;

    private static final String SQL_ROLE
            = "select u.username, r.name as role "
            + "from sso.s_user u "
            + "inner join sso.s_role r on (u.id_role = r.id) "
            //+ "inner join sso.s_role_permission rp on (rp.id_role = r.id) "
            //+ "inner join sso.s_permission p on (rp.id_permission = p.id) "
            + "where u.username = ?";

    private static final String SQL_LOGIN
            = "select u.username as username, u.password as password, u.status as active "
            + "from sso.s_user u "
            + "where username = ?";

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public WebSecurityConfig(AccessDeniedHandler accessDeniedHandler, DataSource dataSource) {
        this.accessDeniedHandler = accessDeniedHandler;
        this.dataSource = dataSource;
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.
                jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery(SQL_LOGIN)
                .authoritiesByUsernameQuery(SQL_ROLE)
                .passwordEncoder(passwordEncoder());

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //http.csrf().disable();
        http
                .authorizeRequests()
                .antMatchers("/", "/beranda", "/beranda/hasil/**").permitAll()
                //                              .antMatchers("/beranda").permitAll()
                //  .antMatchers("/beranda/hasil/**")
                //.antMatchers("/beranda").permitAll()
                .anyRequest().authenticated()
                .and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/login?logout").deleteCookies("JSESSIONID")
                .invalidateHttpSession(true).permitAll()
                .and().formLogin().defaultSuccessUrl("/home")
                .loginPage("/login")//.loginPage("/beranda")
                .permitAll()
                .and()
                .exceptionHandling().accessDeniedHandler(accessDeniedHandler);
        http
                .sessionManagement()
                .maximumSessions(10)
                .maxSessionsPreventsLogin(true)
                .expiredUrl("/expire")
                .sessionRegistry(sessionRegistry());
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/img/favicon.ico")
                .antMatchers("/js/**")
                .antMatchers("/img/**")
                .antMatchers("/img/petugas/**")
                .antMatchers("/css/**")
                .antMatchers("/assets/**")
                .antMatchers("/404")
                .antMatchers("/500")
                .antMatchers("/error")
                //                .antMatchers("/beranda")
                //                .antMatchers("/beranda/hasil/**")
                //                .antMatchers("/")
                .antMatchers("/actuator/**");
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

    @Bean
    public static ServletListenerRegistrationBean httpSessionEventPublisher() {
        return new ServletListenerRegistrationBean(new HttpSessionEventPublisher());
    }

//    @Bean
//    @Override
//    protected UserDetailsService userDetailsService() {
//        UserDetails user
//                = User.withDefaultPasswordEncoder()
//                        .username("root")
//                        .password("root123")
//                        .roles("USER")
//                        .build();
//
//        return new InMemoryUserDetailsManager(user);
//    }
}
