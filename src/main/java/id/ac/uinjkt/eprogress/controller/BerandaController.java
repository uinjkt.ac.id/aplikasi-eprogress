package id.ac.uinjkt.eprogress.controller;

import id.ac.uinjkt.eprogress.dao.ep.TugasDao;
import id.ac.uinjkt.eprogress.dao.ep.TugasKegiatanDao;
import id.ac.uinjkt.eprogress.model.ep.Tugas;
import id.ac.uinjkt.eprogress.model.ep.TugasKegiatan;
import id.ac.uinjkt.eprogress.service.CommonService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Pustipanda
 */
@Controller
public class BerandaController {

    private static final Logger LOG = LoggerFactory.getLogger(BerandaController.class);

    @Autowired
    private CommonService commonService;

    @Autowired
    private TugasDao tugasDao;

    @Autowired
    private TugasKegiatanDao tkd;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        return "beranda";
    }

    @RequestMapping(value = "/beranda", method = RequestMethod.GET)
    public String beranda(Model model) {
        return "beranda";
    }

    @GetMapping("/beranda/hasil")
    public String showList(ModelMap mm, @RequestParam(value = "cari", required = false, defaultValue = "") String cari, @PageableDefault(size = 10) Pageable page) {

        Page<Tugas> result;

        if (!cari.isEmpty()) {
            result = tugasDao.findByNamaContainingIgnoreCase(cari, page);
        } else {
            result = tugasDao.findByNamaContainingIgnoreCase("=", page);
        }

        mm.addAttribute("data", result);
        mm.addAttribute("pencarian", true);
        mm.addAttribute("kueri", cari);

        return "beranda";
    }

    @GetMapping("/beranda/hasil/{id}")
    public String showInfoProgres(ModelMap mm, @PathVariable(value = "id", required = false) Long id, @PageableDefault(size = 14) Pageable page) {

        Optional<Tugas> tugas = tugasDao.findById(id);

        TugasKegiatan tugasKegiatan = tkd.findFirstByTugasIdOrderByModifiedDesc(id);

        mm.addAttribute("progres", commonService.showProgressPercent(id));
        mm.addAttribute("updateterakhir", commonService.convertToDateViaSqlTimestamp(tugasKegiatan.getModified()));
        mm.addAttribute("namatugas", tugas.get().getNama());
        mm.addAttribute("listTugas", tugasDao.findAll(Sort.by("nama")));

        return "beranda :: infoDataRincianPublik";
    }

}
