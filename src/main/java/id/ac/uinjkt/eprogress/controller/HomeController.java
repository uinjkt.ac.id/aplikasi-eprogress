package id.ac.uinjkt.eprogress.controller;

import id.ac.uinjkt.eprogress.dao.sso.UserDao;
import id.ac.uinjkt.eprogress.model.sso.User;
import id.ac.uinjkt.eprogress.service.CurrentUserService;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Pustipanda
 */
@Controller
public class HomeController {

    @Autowired
    private UserDao userDao;

    @Autowired(required = true)
    private PasswordEncoder passwordEncoder;

    private static final Logger LOG = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    private CurrentUserService currentUserService;

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String home(Model model /*, Authentication auth*/) {

        //User u = currentUserService.currentUser(auth);
        //System.out.println("user: " + u.getFullname());
        return "home";
    }

    @RequestMapping(value = "/my_profile", method = RequestMethod.GET)
    public String dashboard(Model model, Authentication auth) {
        User u = currentUserService.currentUser(auth);
        LOG.info("My profile {}", u.getFullname());

        model.addAttribute("user", u);

        return "my_profile";
    }

    @GetMapping(value = "/ubah_password")
    public String formUbahPassword(Model model, Authentication auth) {
        User u = currentUserService.currentUser(auth);
        LOG.info("Form Ubah Password {}", u.getFullname());

        model.addAttribute("user", u);
        return "ubah_password";
    }

    @PostMapping(value = "/ubah_password")
    @ResponseBody
    public Map<String, Object> simpanPassword(Model model, HttpServletRequest request,
            HttpServletResponse response, Authentication auth, @RequestParam("passwordLama") String passLama,
            @RequestParam("passwordBaru") String passBaru, @RequestParam("passwordBaruCek") String passBaruCek) throws IOException {

        Map<String, Object> hasil = new HashMap<>();

        // String status = "error";
        if (auth != null) {
            if (!passLama.isEmpty() && !passBaru.isEmpty() && !passBaruCek.isEmpty()) {
                User u = currentUserService.currentUser(auth);

                if (passwordEncoder.matches(passLama, u.getPassword())) {
                    // LOG.info("Pass lama {}", passwordEncoder.encode(passLama));
                    if (passBaru.equals(passBaruCek)) {
                        Optional<User> userOptional = userDao.findById(u.getId());
                        if (userOptional.isPresent()) {
                            User user = userOptional.get();
                            user.setPassword(passwordEncoder.encode(passBaru));
                            if (!user.getPassword().isEmpty()) {
                                userDao.save(user);
                                //status = "berhasil";
                                hasil.put("status", true);
                                hasil.put("pesan", "berhasil");
                            }

                        }
                    } else {
                    }
                } else {
                }
            }
        }
        return hasil;
        //return "redirect:/ubah_password?" + status;

    }

}
