package id.ac.uinjkt.eprogress.controller.ep;

import id.ac.uinjkt.eprogress.dao.ep.DetilTugasKegiatanDao;
import id.ac.uinjkt.eprogress.dao.ep.KegiatanDao;
import id.ac.uinjkt.eprogress.dao.ep.TugasDao;
import id.ac.uinjkt.eprogress.dao.ep.TugasKegiatanDao;
import id.ac.uinjkt.eprogress.model.ep.DetilTugasKegiatan;
import id.ac.uinjkt.eprogress.model.ep.Kegiatan;
import id.ac.uinjkt.eprogress.model.ep.Tugas;
import id.ac.uinjkt.eprogress.model.ep.TugasKegiatan;
import id.ac.uinjkt.eprogress.service.CommonService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Pustipanda
 */
@Controller
@RequestMapping("/ep/tugaskegiatan/detil")
public class DetilTugasKegiatanController {

    private static final Logger LOG = LoggerFactory.getLogger(DetilTugasKegiatanController.class);

    @Autowired
    private TugasKegiatanDao tkd;

    @Autowired
    private DetilTugasKegiatanDao detilTugasKegiatanDao;

    @Autowired
    private TugasDao tugasDao;

    @Autowired
    private KegiatanDao kegiatanDao;

    @Autowired
    private CommonService commonService;

    @GetMapping("/{id}")
    public String showInfoDaftarRincian(ModelMap mm, @PathVariable(value = "id", required = false) Long id,
            @PageableDefault(size = 14) Pageable page) {

        TugasKegiatan tugasKegiatan;
        List<DetilTugasKegiatan> daftarRincian;

        if (id != null) {
            tugasKegiatan = tkd.findByIdOrderById(id);
            daftarRincian = detilTugasKegiatanDao.findByTugasKegiatanIdOrderById(id);

            if (daftarRincian.isEmpty()) {
                mm.addAttribute("noDataRincian", true);
            } else {
                mm.addAttribute("dataRincianKegiatan", daftarRincian);
                mm.addAttribute("noDataRincian", false);
            }

            mm.addAttribute("o", tugasKegiatan);
        }

        return "ep/tugaskegiatan/list :: infoDaftarRincian";
    }

    @GetMapping("/form")
    public String showForm(@RequestParam(required = false, name = "id") Long idTugasKegiatan,
            @RequestParam(required = false, name = "id2") Long idDetilTugasKegiatan,
            ModelMap mm) {

        DetilTugasKegiatan detilTugasKegiatan = detilTugasKegiatanDao.findByIdOrderByIdAsc(idDetilTugasKegiatan);
        if (detilTugasKegiatan == null) {
            detilTugasKegiatan = new DetilTugasKegiatan();
        }

        if (idTugasKegiatan != null) {

            TugasKegiatan tugasKegiatan = tkd.findByIdOrderById(idTugasKegiatan);
            Optional<Tugas> tugas = tugasDao.findById(tugasKegiatan.getTugas().getId());
            Kegiatan kegiatan = kegiatanDao.findByIdOrderById(tugasKegiatan.getKegiatan().getId());

            mm.addAttribute("kegiatan", kegiatan);
            mm.addAttribute("tugas", tugas.get());

        }

        mm.addAttribute("idtugaskegiatan", idTugasKegiatan);
        mm.addAttribute("detiltugaskegiatan", detilTugasKegiatan);
        return "ep/detiltugaskegiatan/form";
    }

    @PreAuthorize("hasAuthority('Administrator')")
    @PostMapping("/form")
    public String insertForm(@ModelAttribute @Valid DetilTugasKegiatan o,
            @RequestParam(name = "tugas") Long idTugas,
            @RequestParam(name = "kegiatan") Long idKegiatan,
            @RequestParam(name = "tugaskegiatan") TugasKegiatan tugasKegiatan,
            BindingResult errors, ModelMap mm) {

        Optional<Tugas> t = tugasDao.findById(idTugas);

        if (errors.hasErrors()) {
            mm.addAttribute("detiltugaskegiatan", o);
            mm.addAttribute("listTugas", tugasDao.findAll(Sort.by("nama")));
            mm.addAttribute("listKegiatan", kegiatanDao.findAll(Sort.by("id")));
        }

        DetilTugasKegiatan dtk = detilTugasKegiatanDao.findByIdOrderByIdAsc(o.getId());

        if (dtk == null) {
            dtk = new DetilTugasKegiatan();
        }

        dtk.setTugasKegiatan(tugasKegiatan);
        dtk.setDaftarTugasKegiatan(o.getDaftarTugasKegiatan());
        dtk.setStatus(false);

        try {
            detilTugasKegiatanDao.save(dtk);

            long idTugasKegiatan = dtk.getTugasKegiatan().getId();
            commonService.refreshProgres(idTugasKegiatan);

            mm.addAttribute("namatugas", t.get().getNama());
            return "redirect:/ep/tugaskegiatan/list?tugas=" + idTugas + "&status=sukses#kolomKegiatan";

        } catch (Error e) {
            LOG.info("error simpan :" + e);
        }

        return "redirect:/ep/tugaskegiatan/list";
    }

//    @PreAuthorize("hasAuthority('Administrator')")
//    @PostMapping("/form")
//    @ResponseBody
//    public Map<String, Object> insertForm(@ModelAttribute @Valid DetilTugasKegiatan o,
//            @RequestParam(name = "tugas") Long idTugas,
//            @RequestParam(name = "kegiatan") Long idKegiatan,
//            @RequestParam(name = "tugaskegiatan") TugasKegiatan tugasKegiatan,
//            BindingResult errors, ModelMap mm) {
//
//        Optional<Tugas> t = tugasDao.findById(idTugas);
//        Map<String, Object> hasil = new HashMap<>();
//
//        if (errors.hasErrors()) {
//            mm.addAttribute("detiltugaskegiatan", o);
//            mm.addAttribute("listTugas", tugasDao.findAll(Sort.by("nama")));
//            mm.addAttribute("listKegiatan", kegiatanDao.findAll(Sort.by("id")));
//        }
//
//        DetilTugasKegiatan dtk = detilTugasKegiatanDao.findByIdOrderByIdAsc(o.getId());
//        if (dtk == null) {
//            dtk = new DetilTugasKegiatan();
//        }
//
//        dtk.setTugasKegiatan(tugasKegiatan);
//        dtk.setDaftarTugasKegiatan(o.getDaftarTugasKegiatan());
//        dtk.setStatus(false);
//
//        try {
//            detilTugasKegiatanDao.save(dtk);
//
//            mm.addAttribute("namatugas", t.get().getNama());
//            hasil.put("info", "sukses");
//            //return "redirect:/ep/tugaskegiatan/list?tugas=" + idTugas;
//
//        } catch (Error e) {
//            LOG.info("error simpan :" + e);
//        }
//
//        return hasil;
//        //return "redirect:/ep/tugaskegiatan/list";
//    }
    @GetMapping("/delete")
    @ResponseBody
    public Map<String, Object> deleteData(@RequestParam(value = "id", required = false) Long id) {

        Map<String, Object> hasil = new HashMap<>();
        //long idTugas = detilTugasKegiatanDao.findByIdOrderByIdAsc(id).getTugasKegiatan().getTugas().getId();
        String info = "sukses";

        DetilTugasKegiatan detilTugasKegiatan = detilTugasKegiatanDao.findByIdOrderByIdAsc(id);
        long idTugasKegiatan = detilTugasKegiatan.getTugasKegiatan().getId();

        detilTugasKegiatanDao.deleteById(id);

        commonService.refreshProgres(idTugasKegiatan);

        if (detilTugasKegiatanDao.findByIdOrderByIdAsc(id) != null) {
            info = "gagal";
        }

        hasil.put("info", info);
        //return "redirect:/ep/tugaskegiatan/list?tugas=" + idTugas + "#kolomKegiatan";
        //return "/ep/tugaskegiatan/list :: infoSukses";
        return hasil;

    }

    @GetMapping("/update")
    @ResponseBody
    public Map<String, Object> updateData(@RequestParam(value = "id", required = false) Long id, Authentication auth) {

        Map<String, Object> hasil = new HashMap<>();

        String info = "sukses";

        DetilTugasKegiatan detilTugasKegiatan = detilTugasKegiatanDao.findByIdOrderByIdAsc(id);
        detilTugasKegiatan.setStatus(Boolean.TRUE);

        try {
            detilTugasKegiatanDao.save(detilTugasKegiatan);

            long idTugasKegiatan = detilTugasKegiatan.getTugasKegiatan().getId();

            commonService.refreshProgres(idTugasKegiatan);
            commonService.updateLog(idTugasKegiatan, id, "perbarui", auth);

        } catch (Exception e) {
            LOG.info("error terjadi = " + e);
            info = "gagal";
        }

        hasil.put("info", info);

        //return "redirect:/ep/tugaskegiatan/list?tugas=" + idTugas + "#kolomKegiatan";
        return hasil;

    }
}
