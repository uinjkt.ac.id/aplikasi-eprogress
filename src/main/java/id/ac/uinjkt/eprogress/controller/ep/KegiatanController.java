package id.ac.uinjkt.eprogress.controller.ep;

import id.ac.uinjkt.eprogress.dao.ep.KegiatanDao;
import id.ac.uinjkt.eprogress.model.ep.Kegiatan;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Pustipanda
 */
@Controller
@RequestMapping("/ep/kegiatan")
public class KegiatanController {

    private static final Logger LOG = LoggerFactory.getLogger(KegiatanController.class);

    @Autowired
    private KegiatanDao kegiatanDao;

    @ModelAttribute("pageTitle")
    public String pageTitle() {
        return "Daftar Kegiatan";
    }

    @GetMapping("/list")
    public String showList(ModelMap mm, @RequestParam(value = "cari", required = false) String cari, @PageableDefault(size = 10) Pageable page) {

        Page<Kegiatan> result;

        if (cari != null) {
            result = kegiatanDao.findByNamaContainingIgnoreCase(cari, page);
        } else {
            result = kegiatanDao.findAll(page);
        }

        mm.addAttribute("data", result);
        // mm.addAttribute("listStakeholder", sd.findAll(Sort.by("nama")));

        return "ep/kegiatan/list";
    }

    @GetMapping("/form")
    public String showForm(@RequestParam(required = false) Long id, ModelMap mm) {
        Kegiatan kegiatan = new Kegiatan();
        if (id != null) {
            Optional<Kegiatan> o = kegiatanDao.findById(id);
            if (o.isPresent()) {
                kegiatan = o.get();
            }
        }

        //mm.addAttribute("listStakeholder", sd.findAll(Sort.by("nama")));
        mm.addAttribute("kegiatan", kegiatan);

        return "ep/kegiatan/form";
    }

    @PostMapping("/form")
    public String updateForm(@ModelAttribute @Valid Kegiatan o, BindingResult errors, ModelMap mm) {
        if (errors.hasErrors()) {
            mm.addAttribute("kegiatan", o);
            //mm.addAttribute("listStakeholder", sd.findAll(Sort.by("nama")));

        }

        // validasi nama
        Kegiatan oName = kegiatanDao.findByNama(o.getNama());
        if (oName != null && !oName.getId().equals(o.getId())) {
            errors.rejectValue("nama", "nama", "Nama telah digunakan");
            mm.addAttribute("kegiatan", o);
            //mm.addAttribute("listPaket", paketDao.findAll());

            return "ep/kegiatan/form";
        }

        kegiatanDao.save(o);
        return "redirect:/ep/kegiatan/list";
    }

    @GetMapping("/delete")
    public String deleteData(@RequestParam(value = "id", required = false) Long id) {
        kegiatanDao.deleteById(id);

        return "redirect:/ep/kegiatan/list";

    }
}
