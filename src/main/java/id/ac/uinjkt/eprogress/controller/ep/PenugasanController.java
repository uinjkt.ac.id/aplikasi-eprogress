package id.ac.uinjkt.eprogress.controller.ep;

import id.ac.uinjkt.eprogress.dao.ep.PenugasanDao;
import id.ac.uinjkt.eprogress.dao.ep.PetugasDao;
import id.ac.uinjkt.eprogress.dao.ep.TugasDao;
import id.ac.uinjkt.eprogress.model.ep.Penugasan;
import id.ac.uinjkt.eprogress.model.ep.Petugas;
import java.util.LinkedList;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Pustipanda
 */
@Controller
@RequestMapping("/ep/tugaskegiatan/petugas")
public class PenugasanController {

    private static final Logger LOG = LoggerFactory.getLogger(PenugasanController.class);

    @Autowired
    private PetugasDao petugasDao;

    @Autowired
    private TugasDao tugasDao;

    @Autowired
    private PenugasanDao penugasanDao;

    @ModelAttribute("pageTitle")
    public String pageTitle() {
        return "Daftar Penugasan";
    }

    @GetMapping("/list")
    public String showList(ModelMap mm, @RequestParam(value = "cari", required = false) String cari, @PageableDefault(size = 10) Pageable page) {

        Page<Penugasan> result;

        if (cari != null) {
            result = penugasanDao.findByTugasNamaContainingIgnoreCase(cari, page);
        } else {
            result = penugasanDao.findAll(page);
        }

        mm.addAttribute("data", result);
        return "ep/penugasan/list";
    }

    @GetMapping("/form")
    public String showForm(@RequestParam(required = false) Long id, ModelMap mm) {
        Penugasan penugasanBaru = new Penugasan();
        List<Petugas> petugas = petugasDao.findAllByOrderById();
        List<Petugas> petugasU = new LinkedList<>();

        petugasU.addAll(petugas);

        if (id != null) {
            List<Penugasan> penugasanList = penugasanDao.findByTugasIdOrderByPetugasId(id);

            mm.addAttribute("penugasanlist", penugasanList);

            long idcek;
            int i = 0;
            int j = 0;

            for (Penugasan ps : penugasanList) {
                idcek = penugasanList.get(i).getPetugas().getId();
                for (Petugas pg : petugasU) {
                    if (idcek == petugasU.get(j).getId()) {
                        petugasU.remove(j);
                        break;
                    }
                    j++;
                }
                i++;
            }

//            Optional<Penugasan> o = penugasanDao.findByTugasId(id);
//            if (o.isPresent()) {
//                penugasanBaru = o.get();
//            }
        }

        mm.addAttribute("listTugas", tugasDao.findAll());
        mm.addAttribute("listPetugas", petugasU);
        mm.addAttribute("penugasan", penugasanBaru);

        mm.addAttribute("idtugas", id);

        return "ep/penugasan/form";
    }

    @PostMapping("/form")
    public String updateForm(@ModelAttribute @Valid Penugasan o, BindingResult errors, ModelMap mm) {
        if (errors.hasErrors()) {
            mm.addAttribute("penugasan", o);
        }

        // validasi petugas hanya 1
//        Penugasan oName = penugasanDao.findByPetugasId(o.getPetugas().getId());
//        if (oName != null) {
//            if (!oName.getId().equals(o.getId()) && oName.getTugas().getId().equals(o.getTugas().getId()) && oName.getPetugas().getId().equals(o.getPetugas().getId())) {
//                errors.rejectValue("petugas", "petugas", "Petugas ini telah dipilih untuk tugas ini");
//
//                mm.addAttribute("listTugas", tugasDao.findAll());
//                mm.addAttribute("listPetugas", petugasDao.findAll());
//                mm.addAttribute("penugasan", o);
//
//                return "ep/penugasan/form";
//            }
//        }
        if (o.getId() == null) {
            Penugasan p = new Penugasan();
            p.setPetugas(o.getPetugas());
            p.setTugas(o.getTugas());
            penugasanDao.save(p);
        } else {
            penugasanDao.save(o);
        }

        return "redirect:/ep/tugaskegiatan/list?tugas=" + o.getTugas().getId();
    }

    @GetMapping("/delete")
    public String deleteData(@RequestParam(value = "id", required = false) Long id) {
        penugasanDao.deleteById(id);

        return "redirect:/ep/penugasan/list";

    }
}
