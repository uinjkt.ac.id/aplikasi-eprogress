package id.ac.uinjkt.eprogress.controller.ep;

import id.ac.uinjkt.eprogress.dao.ep.PetugasDao;
import id.ac.uinjkt.eprogress.model.ep.Petugas;
import java.io.File;
import java.io.IOException;
import java.util.Optional;
import javax.servlet.ServletContext;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Pustipanda
 */
@Controller
@RequestMapping("/ep/petugas")
public class PetugasController {

    private static final Logger LOG = LoggerFactory.getLogger(PetugasController.class);

    @Autowired
    private ServletContext session;

    @Autowired
    private PetugasDao petugasDao;

    @ModelAttribute("pageTitle")
    public String pageTitle() {
        return "Daftar Petugas";
    }

    @GetMapping("/list")
    public String showList(ModelMap mm, @RequestParam(value = "cari", required = false) String cari, @PageableDefault(size = 10) Pageable page) {

        Page<Petugas> result;

        if (cari != null) {
            result = petugasDao.findByNamaContainingIgnoreCase(cari, page);
        } else {
            result = petugasDao.findAll(page);
        }

        mm.addAttribute("data", result);
        return "ep/petugas/list";
    }

    @GetMapping("/form")
    public String showForm(@RequestParam(required = false) Long id, ModelMap mm) {
        Petugas petugasBaru = new Petugas();
        if (id != null) {
            Optional<Petugas> o = petugasDao.findById(id);
            if (o.isPresent()) {
                petugasBaru = o.get();
            }
        }

        //mm.addAttribute("listPaket", paketDao.findAll());
        mm.addAttribute("petugas", petugasBaru);

        return "ep/petugas/form";
    }

    @PostMapping("/form")
    public String updateForm(@ModelAttribute @Valid Petugas o, BindingResult errors, @RequestParam(name = "foto", defaultValue = "") MultipartFile foto, ModelMap mm) throws IOException {
        if (errors.hasErrors()) {
            mm.addAttribute("petugas", o);
        }

        // validasi no absen
        Petugas oNomorAbsen = petugasDao.findByNomorAbsen(o.getNomorAbsen());
        if (oNomorAbsen != null && !oNomorAbsen.getId().equals(o.getId())) {
            errors.rejectValue("nomorAbsen", "nomorAbsen", "No Absen telah digunakan");
            mm.addAttribute("petugas", o);

            return "ep/petugas/form";
        }
        if (!foto.isEmpty()) {

            o.setFoto(foto.getOriginalFilename().replaceAll(" ", "_").toLowerCase());

            String namaFile = foto.getName();
            String jenisFile = foto.getContentType();
            String namaAsli = foto.getOriginalFilename();
            String namaAkhir = namaAsli.replaceAll(" ", "_").toLowerCase();
            Long ukuranFile = foto.getSize();

//        System.out.println("nama file =" + namaFile);
//        System.out.println("jenis file =" + jenisFile);
//        System.out.println("nama asli =" + namaAsli);
//        System.out.println("ukuran file =" + ukuranFile);
            String lokasiPath = "/upload";
            String lokasiTomcat = session.getRealPath(lokasiPath);
//        System.out.println("Lokasi tomcat dijalankan: " + lokasiTomcat);
            //String lokasiTujuan = lokasiTomcat + File.separator;

            //String homeFolder = System.getProperty("user.home");
            String homeFolder = System.getProperty("user.dir");
            String lokasiTujuan = homeFolder + File.separator + "\\src\\main\\resources\\static\\img\\petugas" + File.separator;
            File folderTujuan = new File(lokasiTujuan);
            if (!folderTujuan.exists()) {
                folderTujuan.mkdirs();
            }
            File tujuan = new File(lokasiTujuan + File.separator + namaAkhir);

            foto.transferTo(tujuan);
            LOG.info("File sudah dicopy ke: " + tujuan.getAbsolutePath());
        }
        petugasDao.save(o);

        return "redirect:/ep/petugas/list";
    }

    @GetMapping("/delete")
    public String deleteData(@RequestParam(value = "id", required = false) Long id) {
        petugasDao.deleteById(id);

        return "redirect:/ep/petugas/list";

    }
}
