package id.ac.uinjkt.eprogress.controller.ep;

import id.ac.uinjkt.eprogress.dao.ep.StakeholderDao;
import id.ac.uinjkt.eprogress.model.ep.Stakeholder;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Pustipanda
 */
@Controller
@RequestMapping("/ep/stakeholder")
public class StakeholderController {

    private static final Logger LOG = LoggerFactory.getLogger(StakeholderController.class);

    @Autowired
    private StakeholderDao sd;

    @ModelAttribute("pageTitle")
    public String pageTitle() {
        return "Daftar Stakeholder";
    }

    @GetMapping("/list")
    public String showList(ModelMap mm, @RequestParam(value = "cari", required = false) String cari, @PageableDefault(size = 10) Pageable page) {

        Page<Stakeholder> result;

        if (cari != null) {
            result = sd.findByNamaContainingIgnoreCase(cari, page);
        } else {
            result = sd.findAll(page);
        }

        mm.addAttribute("data", result);
        return "ep/stakeholder/list";
    }

    @GetMapping("/form")
    public String showForm(@RequestParam(required = false) Long id, ModelMap mm) {
        Stakeholder stakeBaru = new Stakeholder();
        if (id != null) {
            Optional<Stakeholder> o = sd.findById(id);
            if (o.isPresent()) {
                stakeBaru = o.get();
            }
        }

        //mm.addAttribute("listPaket", paketDao.findAll());
        mm.addAttribute("stakeholder", stakeBaru);

        return "ep/stakeholder/form";
    }

    @PostMapping("/form")
    public String updateForm(@ModelAttribute @Valid Stakeholder o, BindingResult errors, ModelMap mm) {
        if (errors.hasErrors()) {
            mm.addAttribute("stakeholder", o);
            //mm.addAttribute("listPaket", paketDao.findAll());

            //return "sso/role/form";
        }

        // validasi nama
        Stakeholder oName = sd.findByNama(o.getNama());
        if (oName != null && !oName.getId().equals(o.getId())) {
            errors.rejectValue("nama", "nama", "Nama telah digunakan");
            mm.addAttribute("stakeholder", o);
            //mm.addAttribute("listPaket", paketDao.findAll());

            return "ep/stakeholder/form";
        }

        sd.save(o);
        return "redirect:/ep/stakeholder/list";
    }

    @GetMapping("/delete")
    public String deleteData(@RequestParam(value = "id", required = false) Long id) {
        sd.deleteById(id);

        return "redirect:/ep/stakeholder/list";

    }

}
