package id.ac.uinjkt.eprogress.controller.ep;

import id.ac.uinjkt.eprogress.dao.ep.StakeholderDao;
import id.ac.uinjkt.eprogress.dao.ep.TugasDao;
import id.ac.uinjkt.eprogress.model.ep.Tugas;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Pustipanda
 */
@Controller
@RequestMapping("/ep/tugas")
public class TugasController {

    private static final Logger LOG = LoggerFactory.getLogger(TugasController.class);

    @Autowired
    private TugasDao tugasDao;

    @Autowired
    private StakeholderDao sd;

    @ModelAttribute("pageTitle")
    public String pageTitle() {
        return "Daftar Tugas";
    }

    @GetMapping("/list")
    public String showList(ModelMap mm, @RequestParam(value = "cari", required = false) String cari, @PageableDefault(size = 10) Pageable page) {

        Page<Tugas> result;

        if (cari != null) {
            result = tugasDao.findByNamaContainingIgnoreCase(cari, page);
        } else {
            result = tugasDao.findAll(page);
        }

        mm.addAttribute("data", result);
        mm.addAttribute("listStakeholder", sd.findAll(Sort.by("nama")));

        return "ep/tugas/list";
    }

    @GetMapping("/form")
    public String showForm(@RequestParam(required = false) Long id, ModelMap mm) {
        Tugas tugas = new Tugas();
        if (id != null) {
            Optional<Tugas> o = tugasDao.findById(id);
            if (o.isPresent()) {
                tugas = o.get();
            }
        }

        mm.addAttribute("listStakeholder", sd.findAll(Sort.by("nama")));
        mm.addAttribute("tugas", tugas);

        return "ep/tugas/form";
    }

    @PostMapping("/form")
    public String updateForm(@ModelAttribute @Valid Tugas o, BindingResult errors, ModelMap mm) {
        if (errors.hasErrors()) {
            mm.addAttribute("tugas", o);
            mm.addAttribute("listStakeholder", sd.findAll(Sort.by("nama")));

        }

        // validasi nama
        Tugas oName = tugasDao.findByNama(o.getNama());
        if (oName != null && !oName.getId().equals(o.getId())) {
            errors.rejectValue("nama", "nama", "Nama telah digunakan");
            mm.addAttribute("tugas", o);
            //mm.addAttribute("listPaket", paketDao.findAll());

            return "ep/tugas/form";
        }

        tugasDao.save(o);
        return "redirect:/ep/tugas/list";
    }

    @GetMapping("/delete")
    public String deleteData(@RequestParam(value = "id", required = false) Long id) {
        tugasDao.deleteById(id);

        return "redirect:/ep/tugas/list";

    }
}
