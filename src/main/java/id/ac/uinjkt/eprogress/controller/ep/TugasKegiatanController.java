package id.ac.uinjkt.eprogress.controller.ep;

import id.ac.uinjkt.eprogress.dao.ep.KegiatanDao;
import id.ac.uinjkt.eprogress.dao.ep.PenugasanDao;
import id.ac.uinjkt.eprogress.dao.ep.PetugasDao;
import id.ac.uinjkt.eprogress.dao.ep.TugasDao;
import id.ac.uinjkt.eprogress.dao.ep.TugasKegiatanDao;
import id.ac.uinjkt.eprogress.model.ep.Kegiatan;
import id.ac.uinjkt.eprogress.model.ep.Penugasan;
import id.ac.uinjkt.eprogress.model.ep.Tugas;
import id.ac.uinjkt.eprogress.model.ep.TugasKegiatan;
import id.ac.uinjkt.eprogress.model.sso.User;
import id.ac.uinjkt.eprogress.service.CommonService;
import id.ac.uinjkt.eprogress.service.CurrentUserService;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Pustipanda
 */
@Controller
@RequestMapping("/ep/tugaskegiatan")
public class TugasKegiatanController {
    
    private static final Logger LOG = LoggerFactory.getLogger(TugasKegiatanController.class);
    
    @Autowired
    private CommonService commonService;
    
    @Autowired
    private TugasDao tugasDao;
    
    @Autowired
    private TugasKegiatanDao tkd;
    
    @Autowired
    private PenugasanDao penugasanDao;
    
    @Autowired
    private KegiatanDao kd;
    
    @Autowired
    private PetugasDao petugasDao;
    
    @Autowired
    private CurrentUserService currentUserService;
    
    @ModelAttribute("pageTitle")
    public String pageTitle() {
        return "Daftar Tugas Kegiatan";
    }
    
    @GetMapping("/list")
    public String showList(ModelMap mm, @RequestParam(value = "tugas", required = false) Long tugas, @PageableDefault(size = 10) Pageable page) {
        
        Page<TugasKegiatan> result;
        List<Penugasan> resultPenugasan = null;
        
        if (tugas == null) {
            result = tkd.findAll(page);
        } else {
            result = tkd.findByTugasIdOrderByKegiatanId(tugas, page);
            resultPenugasan = penugasanDao.findByTugasIdOrderByTugasId(tugas);
            
            List<TugasKegiatan> o = tkd.findByTugasId(tugas);
            Optional<Tugas> namaTugas = tugasDao.findById(tugas);
            
            if (namaTugas != null) {
                mm.addAttribute("namatugas", namaTugas.get().getNama());
            }
            
            if (!o.isEmpty()) {
//                mm.addAttribute("namatugas", o.get(0).getTugas().getNama());

                TugasKegiatan tugasKegiatan = tkd.findFirstByTugasIdOrderByModifiedDesc(tugas);
                mm.addAttribute("updateterakhir", commonService.convertToDateViaSqlTimestamp(tugasKegiatan.getModified()));
                
            }
            mm.addAttribute("idtugas", tugas);
        }

        mm.addAttribute("progres", commonService.showProgressPercent(tugas));
        
        if (result.isEmpty()) {
            if (tugas != null) {
                mm.addAttribute("noData", true);
            }
        }
        
        mm.addAttribute("listRincian", true);
        mm.addAttribute("data", result);
        mm.addAttribute("listTugas", tugasDao.findAll(Sort.by("nama")));
        mm.addAttribute("dataPenugasan", resultPenugasan);
        
        return "ep/tugaskegiatan/list";
    }
    
    @GetMapping("/form")
    public String showForm(@RequestParam(required = false, name = "id") Long id,
            @RequestParam(required = false, name = "id2") Long id2, ModelMap mm) {
        
        TugasKegiatan tugasKegiatan = new TugasKegiatan();
        List<Kegiatan> kegiatan = kd.findAllByOrderById();
        List<Kegiatan> kegiatanU = new LinkedList<>();
        
        kegiatanU.addAll(kegiatan);
        
        if (id != null) {
            Optional<TugasKegiatan> o = tkd.findById(id);
            if (o.isPresent()) {
                tugasKegiatan = o.get();
                mm.addAttribute("kunciform", true);
            }
        }
        if (id2 != null) {
            List<TugasKegiatan> tugasKegiatanList = tkd.findByTugasIdOrderByKegiatanIdAsc(id2);
            mm.addAttribute("tugaskegiatanlist", tugasKegiatanList);
            
            long idcek;
            int i = 0;
            int j = 0;
            
            for (TugasKegiatan tk : tugasKegiatanList) {
                idcek = tugasKegiatanList.get(i).getKegiatan().getId();
                for (Kegiatan kg : kegiatanU) {
                    if (idcek == kegiatanU.get(j).getId()) {
                        kegiatanU.remove(j);
                        break;
                    }
                    j++;
                }
                i++;
            }

//            for (Kegiatan kg : kegiatan) {
//
//                for (TugasKegiatan tk : tugasKegiatanList) {
//                    idcek = tugasKegiatanList.get(i).getKegiatan().getId();
//                    if (!kegiatanU.isEmpty()) {
//
//                        if (kegiatanU.get(j).getId() == idcek) {
//                            kegiatanU.remove(j);
//                        }
//
//                    }
//                    i++;
//
//                }
//                //kg.setId(kegiatan.get(j).getId());
//                i = 0;
//                if (!kegiatanU.isEmpty()) {
//                    j = kegiatanU.size() - 2;
//                    j++;
//                }
//            }
        }
        
        mm.addAttribute("listTugas", tugasDao.findAll(Sort.by("nama")));
        mm.addAttribute("listKegiatan", kegiatanU); //findByKegiatanNull()
        mm.addAttribute("tugaskegiatan", tugasKegiatan);
        
        mm.addAttribute("idtugas", id2);
        
        return "ep/tugaskegiatan/form";
    }
    
    @PreAuthorize("hasAuthority('Administrator')")
    @PostMapping("/form")
    public String updateForm(@ModelAttribute @Valid TugasKegiatan o, BindingResult errors, ModelMap mm) {
        if (errors.hasErrors()) {
            mm.addAttribute("tugaskegiatan", o);
            mm.addAttribute("listTugas", tugasDao.findAll(Sort.by("nama")));
            mm.addAttribute("listKegiatan", kd.findAll(Sort.by("id")));
        }
        
        if (o.getProgres() == null) {
            o.setProgres(0L);
        }
        //o.getProgres() == null ? o.setProgres((long)0) : o.getProgres();

        // validasi nama
        /*
        TugasKegiatan oName = tkd.findByNama(o.ge.getNama());
        if (oName != null && !oName.getId().equals(o.getId())) {
            errors.rejectValue("nama", "nama", "Nama telah digunakan");
            mm.addAttribute("tugas", o);
            //mm.addAttribute("listPaket", paketDao.findAll());

            return "ep/tugas/form";
        }
         */
        try {
            if (o.getId() == null) {
                TugasKegiatan p = new TugasKegiatan();
                p.setKegiatan(o.getKegiatan());
                p.setTugas(o.getTugas());
                p.setProgres(o.getProgres());
                p.setRincian(o.getRincian());
                
                tkd.save(p);
            } else {
                tkd.save(o);
            }
            
            mm.addAttribute("namatugas", o.getTugas().getNama());
            return "redirect:/ep/tugaskegiatan/list?tugas=" + o.getTugas().getId() + "&status=sukses#kolomKegiatan";
            
        } catch (Error e) {
            LOG.info("error simpan :" + e);
        }
        
        return "redirect:/ep/tugaskegiatan/list";
    }
    
    @PreAuthorize("hasAuthority('Petugas')")
    @PostMapping("/update")
    public String updateFormPetugas(@RequestParam(name = "id", required = false) Long id,
            @RequestParam(name = "progres", required = false) Long progres,
            @RequestParam(name = "rincian", required = false) String rincian, Authentication auth, ModelMap mm) {
        
        User u = currentUserService.currentUser(auth);
        LOG.info("My profile {}", u.getFullname());
        
        Optional<TugasKegiatan> tugasKegiatan = tkd.findById(id);
        if (tugasKegiatan != null) {
            
            try {
                tkd.save(tugasKegiatan.get());
                mm.addAttribute("namatugas", tugasKegiatan.get().getTugas().getNama());
                
                return "redirect:/ep/tugaskegiatan/list?tugas=" + tugasKegiatan.get().getTugas().getId() + "&status=sukses#kolomKegiatan";
                //return "redirect:/ep/tugaskegiatan/list?tugas=" + tugasKegiatan.get().getTugas().getId();

            } catch (Error e) {
                LOG.info("error update :" + e);
            }
            
        }
        
        return "redirect:/ep/tugaskegiatan/list";
    }
    
    @GetMapping("/delete")
    @ResponseBody
    public Map<String, Object> deleteData(@RequestParam(value = "id", required = false) Long id) {
        
        Map<String, Object> hasil = new HashMap<>();
        //long idTugas = tkd.findByIdOrderById(id).getTugas().getId();
        String info = "sukses";
        
        tkd.deleteById(id);
        if (tkd.findByIdOrderById(id) != null) {
            info = "gagal";
        }
        hasil.put("info", info);

        //return "redirect:/ep/tugaskegiatan/list?tugas=" + idTugas + "#kolomKegiatan";
        return hasil;
        
    }
    
    @GetMapping("/delete/petugas")
    @ResponseBody
    public Map<String, Object> deleteDataPetugas(@RequestParam(value = "id", required = false) Long id) {
        
        Map<String, Object> hasil = new HashMap<>();
        //long idTugas = tkd.findByIdOrderById(id).getTugas().getId();
        String info = "sukses";
        
        penugasanDao.deleteById(id);
        
        if (penugasanDao.findById(id).isPresent()) {
            info = "gagal";
        }
        hasil.put("info", info);

        //return "redirect:/ep/tugaskegiatan/list?tugas=" + idTugas + "#kolomKegiatan";
        return hasil;
        
    }
    
    @GetMapping("/{id}")
    public String showInfoLog(ModelMap mm, @PathVariable(value = "id", required = false) Long id, @PageableDefault(size = 14) Pageable page) {
        Optional<TugasKegiatan> result;
        TugasKegiatan hasil;
        if (id != null) {
            mm.addAttribute("id", id);
            result = tkd.findById(id);
            hasil = result.get();
            
        } else {
            hasil = null;
        }
        
        mm.addAttribute("dataTugasKegiatan", hasil);
        
        return "ep/tugaskegiatan/list :: infoDataLog";
    }
    
}
