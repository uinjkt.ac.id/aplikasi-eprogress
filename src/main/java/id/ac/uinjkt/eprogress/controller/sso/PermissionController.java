package id.ac.uinjkt.eprogress.controller.sso;


import id.ac.uinjkt.eprogress.dao.sso.PermissionDao;
import id.ac.uinjkt.eprogress.model.sso.Permission;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author Pustipanda
 */
@Controller
@RequestMapping("/sso/permission")
public class PermissionController {

    @Autowired
    private PermissionDao permissionDao;

    @ModelAttribute("pageTitle")
    public String pageTitle() {
        return "Daftar Permission";
    }

    @GetMapping("/list")
    public String showList(ModelMap mm, @RequestParam(value = "key", required = false) String key, @PageableDefault(size = 10) Pageable page) {
        Page<Permission> result;

        if (key != null) {
            result = permissionDao.findByValueContainingIgnoreCase(key, page);
            mm.addAttribute("key", key);
        } else {
            result = permissionDao.findAll(page);
        }

        mm.addAttribute("data", result);

        return "sso/permission/list";
    }

    @GetMapping("/form")
    public String showForm(@RequestParam(required = false) String id, ModelMap mm) {
        Permission permission = new Permission();
        if (StringUtils.hasText(id)) {
            Optional<Permission> o = permissionDao.findById(id);
            if (o.isPresent()) {
                permission = o.get();
            }
        }

        mm.addAttribute("permission", permission);

        return "sso/permission/form";
    }

    @PostMapping("/form")
    public String updateForm(@ModelAttribute @Valid Permission p, BindingResult errors, ModelMap mm) {
        if (errors.hasErrors()) {
            mm.addAttribute("permission", p);

            return "sso/permission/form";
        }

        // validasi label
        Permission pLabel = permissionDao.findByLabel(p.getLabel());
        if (pLabel != null && !pLabel.getId().equals(p.getId())) {
            errors.rejectValue("label", "label", "Permission Label telah digunakan");
            mm.addAttribute("permission", p);

            return "sso/permission/form";
        }

        // validasi value
        Permission pValue = permissionDao.findByValue(p.getValue());
        if (pValue != null && !pValue.getId().equals(p.getId())) {
            errors.rejectValue("value", "value", "Permission Value telah digunakan");
            mm.addAttribute("permission", p);

            return "sso/permission/form";
        }

        permissionDao.save(p);
        return "redirect:/sso/permission/list";
    }

    @GetMapping("/delete")
    public String deleteData(@RequestParam(value = "id", required = false) String id) {
        permissionDao.deleteById(id);

        return "redirect:/sso/permission/list";

    }
}
