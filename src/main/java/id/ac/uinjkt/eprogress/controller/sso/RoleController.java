package id.ac.uinjkt.eprogress.controller.sso;

import id.ac.uinjkt.eprogress.dao.sso.PermissionDao;
import id.ac.uinjkt.eprogress.dao.sso.RoleDao;
import id.ac.uinjkt.eprogress.model.sso.Role;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Pustipanda
 */
@Controller
@RequestMapping("/sso/role")
public class RoleController {

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private PermissionDao permissionDao;

    @ModelAttribute("pageTitle")
    public String pageTitle() {
        return "Daftar Role";
    }

    @GetMapping("/list")
    public String showList(ModelMap mm, @RequestParam(value = "key", required = false) String key, @PageableDefault(size = 10) Pageable page) {
        Page<Role> result;

        if (key != null) {
            result = roleDao.findByNameContainingIgnoreCase(key, page);
        } else {
            result = roleDao.findAll(page);
        }

        mm.addAttribute("data", result);

        return "sso/role/list";
    }

    @GetMapping("/form")
    public String showForm(@RequestParam(required = false) String id, ModelMap mm) {
        Role role = new Role();
        if (StringUtils.hasText(id)) {
            Optional<Role> o = roleDao.findById(id);
            if (o.isPresent()) {
                role = o.get();
            }
        }

        mm.addAttribute("listPermission", permissionDao.findAll());
        mm.addAttribute("role", role);

        return "sso/role/form";
    }

    @PostMapping("/form")
    public String updateForm(@ModelAttribute @Valid Role o, BindingResult errors, ModelMap mm) {
        if (errors.hasErrors()) {
            mm.addAttribute("role", o);
            mm.addAttribute("listRole", roleDao.findAll());

            //return "sso/role/form";
        }

        // validasi nama
        Role oName = roleDao.findByName(o.getName());
        if (oName != null && !oName.getId().equals(o.getId())) {
            errors.rejectValue("name", "name", "Nama role telah digunakan");
            mm.addAttribute("role", o);
            mm.addAttribute("listPermission", permissionDao.findAll());

            return "sso/role/form";
        }

        roleDao.save(o);
        return "redirect:/sso/role/list";
    }

    @GetMapping("/delete")
    public String deleteData(@RequestParam(value = "id", required = false) String id) {
        roleDao.deleteById(id);

        return "redirect:/sso/role/list";

    }
}
