package id.ac.uinjkt.eprogress.controller.sso;

import id.ac.uinjkt.eprogress.dao.ep.PetugasDao;
import id.ac.uinjkt.eprogress.dao.ep.StakeholderDao;
import id.ac.uinjkt.eprogress.dao.sso.RoleDao;
import id.ac.uinjkt.eprogress.dao.sso.UserDao;
import id.ac.uinjkt.eprogress.model.sso.User;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Bang Pardi
 */
@Controller
@RequestMapping("/sso/user")
public class UserController {

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private StakeholderDao stakeholderDao;

    @Autowired
    private PetugasDao petugasDao;

    @Value("app.password.default")
    private String password;

    @ModelAttribute("pageTitle")
    public String pageTitle() {
        return "Daftar Pengguna";
    }

    @Autowired(required = true)
    private PasswordEncoder passwordEncoder;

    @GetMapping("/list")
    public String showList(ModelMap mm, @RequestParam(value = "key", required = false) String key, @PageableDefault(size = 10) Pageable page) {
        Page<User> result;

        if (key != null) {
            result = userDao.findByUsernameContainingIgnoreCase(key, page);
            mm.addAttribute("key", key);
        } else {
            result = userDao.findAll(page);
        }

        mm.addAttribute("data", result);

        return "sso/user/list";
    }

    @GetMapping("/form")
    public String showForm(@RequestParam(required = false) String id, ModelMap mm) {
        User user = new User();
        if (StringUtils.hasText(id)) {
            Optional<User> o = userDao.findById(id);
            if (o.isPresent()) {
                user = o.get();
            }
        }

        mm.addAttribute("listStakeholder", stakeholderDao.findAll());
        mm.addAttribute("listPetugas", petugasDao.findAll());
        mm.addAttribute("listRole", roleDao.findAll());
        mm.addAttribute("user", user);

        return "sso/user/form";
    }

    @PostMapping("/form")
    public String updateForm(@ModelAttribute
            @Valid User o, BindingResult errors, ModelMap mm) {
        if (errors.hasErrors()) {
            mm.addAttribute("user", o);

            return "sso/user/form";
        }

        // validasi nama
        User oUsername = userDao.findByUsername(o.getUsername());
        if (oUsername != null && !oUsername.getId().equals(o.getId())) {
            errors.rejectValue("username", "username", "Username telah digunakan");
            mm.addAttribute("user", o);
            mm.addAttribute("listStakeholder", stakeholderDao.findAll());
            mm.addAttribute("listPetugas", petugasDao.findAll());
            mm.addAttribute("listRole", roleDao.findAll());

            return "sso/user/form";
        }

        if (StringUtils.hasText(o.getPassword())) {
            password = o.getPassword();
        }
        o.setPassword(passwordEncoder.encode(password));
        userDao.save(o);
        return "redirect:/sso/user/list";
    }

    @GetMapping("/delete")
    public String deleteData(@RequestParam(value = "id", required = false) String id) {
        userDao.deleteById(id);

        return "redirect:/sso/user/list";

    }
}
