package id.ac.uinjkt.eprogress.dao.ep;

import id.ac.uinjkt.eprogress.model.ep.DetilTugasKegiatan;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author Pustipanda
 */
public interface DetilTugasKegiatanDao extends PagingAndSortingRepository<DetilTugasKegiatan, Long> {

    public List<DetilTugasKegiatan> findByTugasKegiatanIdOrderById(Long idTugasKegiatan);

    public List<DetilTugasKegiatan> findByTugasKegiatanIdAndStatusTrueOrderById(Long idTugasKegiatan);

    DetilTugasKegiatan findByIdOrderByIdAsc(Long id);

    Long countByTugasKegiatanIdAndStatusTrue(Long idTugasKegiatan);
    
    Long countByTugasKegiatanId(Long idTugasKegiatan);
}
