package id.ac.uinjkt.eprogress.dao.ep;

import id.ac.uinjkt.eprogress.model.ep.Kegiatan;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author Pustipanda
 */
public interface KegiatanDao extends PagingAndSortingRepository<Kegiatan, Long> {

    public Page<Kegiatan> findByNamaContainingIgnoreCase(String nama, Pageable page);

    public List<Kegiatan> findAllByOrderById();

    Kegiatan findByNama(String nama);

    Kegiatan findByIdOrderById(Long id);

}
