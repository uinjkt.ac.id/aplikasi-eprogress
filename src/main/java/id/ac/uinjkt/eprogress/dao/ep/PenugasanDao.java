package id.ac.uinjkt.eprogress.dao.ep;

import id.ac.uinjkt.eprogress.model.ep.Penugasan;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author Pustipanda
 */
public interface PenugasanDao extends PagingAndSortingRepository<Penugasan, Long> {

    public Page<Penugasan> findByTugasNamaContainingIgnoreCase(String nama, Pageable page);

    List<Penugasan> findByTugasIdOrderByTugasId(Long idTugas);

    List<Penugasan> findByTugasIdOrderByPetugasId(Long idTugas);

    Penugasan findByPetugasId(Long idPetugas);

    Optional<Penugasan> findByTugasId(Long idTugas);

}
