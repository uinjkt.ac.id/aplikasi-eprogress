package id.ac.uinjkt.eprogress.dao.ep;

import id.ac.uinjkt.eprogress.model.ep.Petugas;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author Pustipanda
 */
public interface PetugasDao extends PagingAndSortingRepository<Petugas, Long> {

    public Page<Petugas> findByNamaContainingIgnoreCase(String nama, Pageable page);

    Petugas findByNomorAbsen(String nomorAbsen);

    List<Petugas> findAllByOrderById();

}
