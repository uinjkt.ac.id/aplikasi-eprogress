
package id.ac.uinjkt.eprogress.dao.ep;

import id.ac.uinjkt.eprogress.model.ep.Stakeholder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author Pustipanda
 */
public interface StakeholderDao extends PagingAndSortingRepository<Stakeholder, Long>{
    
    public Page<Stakeholder> findByNamaContainingIgnoreCase(String nama, Pageable page);
    
    Stakeholder findByNama(String nama);
}
