package id.ac.uinjkt.eprogress.dao.ep;

import org.springframework.data.repository.PagingAndSortingRepository;
import id.ac.uinjkt.eprogress.model.ep.Tugas;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author Pustipanda
 */
public interface TugasDao extends PagingAndSortingRepository<Tugas, Long> {

    public Page<Tugas> findByNamaContainingIgnoreCase(String nama, Pageable page);

    Tugas findByNama(String nama);
}
