package id.ac.uinjkt.eprogress.dao.ep;

import id.ac.uinjkt.eprogress.model.ep.TugasKegiatan;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author Pustipanda
 */
public interface TugasKegiatanDao extends PagingAndSortingRepository<TugasKegiatan, Long>{
     
    public Page<TugasKegiatan> findByTugasIdOrderByTugasId(Long idTugas, Pageable page);
    
    public Page<TugasKegiatan> findByTugasNamaContainingIgnoreCase(String nama, Pageable page);
    
    public Page<TugasKegiatan> findByTugasIdOrderByKegiatanId(Long idTugas, Pageable page);
    
    public List<TugasKegiatan> findByTugasId(Long idTugas);
    
    public List<TugasKegiatan> findByTugasIdOrderByKegiatanIdAsc(Long idTugas);
    
    public List<TugasKegiatan> findByTugasIdAndKegiatanId(Long idTugas, Long idKegiatan);
    
    TugasKegiatan findFirstByTugasIdOrderByModifiedDesc(Long idTugas);
        
    TugasKegiatan findByTugasIdOrderById(Long idTugas);
    
    TugasKegiatan findByIdOrderById(Long id);
    
    Long countByTugasId(Long idTugas);
    
    TugasKegiatan findByKegiatanIdAndTugasId(Long idKegiatan, Long idTugas);
    
}
