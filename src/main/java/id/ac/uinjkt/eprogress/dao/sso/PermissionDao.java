package id.ac.uinjkt.eprogress.dao.sso;


import id.ac.uinjkt.eprogress.model.sso.Permission;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author Pustipanda
 */
public interface PermissionDao extends PagingAndSortingRepository<Permission, String> {

    Permission findByLabel(String label);

    Permission findByValue(String value);

    public Page<Permission> findByValueContainingIgnoreCase(String value, Pageable page);

}
