package id.ac.uinjkt.eprogress.dao.sso;

import id.ac.uinjkt.eprogress.model.sso.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author Pustipanda
 */
public interface RoleDao extends PagingAndSortingRepository<Role, String> {

    Role findByName(String role);

    public Page<Role> findByNameContainingIgnoreCase(String name, Pageable page);
}
