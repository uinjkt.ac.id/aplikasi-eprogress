package id.ac.uinjkt.eprogress.dao.sso;


import id.ac.uinjkt.eprogress.model.ep.Stakeholder;
import id.ac.uinjkt.eprogress.model.sso.User;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author Pustipanda
 */
public interface UserDao extends PagingAndSortingRepository<User, String> {

    public Page<User> findByUsernameContainingIgnoreCase(String username, Pageable page);

    User findByUsername(String username);

    @Override
    public Optional<User> findById(String id);
    
    public Optional<User> findByStakeholder(Stakeholder stakeholder);
}
