package id.ac.uinjkt.eprogress.model.ep;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author Pustipanda
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(schema = "ep", name = "penugasan")
public class Penugasan implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, unique = true, nullable = false)
    @Getter
    @Setter
    private Long id;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @Fetch(FetchMode.SELECT)
    @JoinColumn(name = "petugas")
    @Getter
    @Setter
    private Petugas petugas;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @Fetch(FetchMode.SELECT)
    @JoinColumn(name = "tugas")
    @Getter
    @Setter
    private Tugas tugas;

    @CreatedBy
    @Column(name = "created_by", updatable = false)
    @Setter
    @Getter
    private String createdBy;

    @CreatedDate
    @Column(name = "created", updatable = false)
    @Setter
    @Getter
    private LocalDateTime created;

    @LastModifiedBy
    @Column(name = "modified_by")
    @Setter
    @Getter
    private String modifiedBy;

    @LastModifiedDate
    @Column(name = "modified")
    @Setter
    @Getter
    private LocalDateTime modified;

}
