package id.ac.uinjkt.eprogress.model.ep;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author Pustipanda
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(schema = "ep", name = "petugas")
public class Petugas implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, unique = true, nullable = false)
    @Getter
    @Setter
    private Long id;

    @Column(name = "nama", length = 50)
    @Getter
    @Setter
    private String nama;

    @Column(name = "nomor_absen", length = 50)
    @Getter
    @Setter
    private String nomorAbsen;

    @Column(name = "jenis_kelamin", length = 50)
    @Getter
    @Setter
    private String jenisKelamin;

    @Column(name = "foto", length = 50)
    @Getter
    @Setter
    private String foto;

    @Column(name = "divisi", length = 50)
    @Getter
    @Setter
    private String divisi;

}
