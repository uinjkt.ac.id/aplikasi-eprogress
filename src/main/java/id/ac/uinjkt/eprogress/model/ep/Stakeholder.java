package id.ac.uinjkt.eprogress.model.ep;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author Pustipanda
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(schema = "ep", name = "stakeholder")
public class Stakeholder implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, unique = true, nullable = false)
    @Getter
    @Setter
    private Long id;

    @Column(name = "nama", length = 50)
    @Getter
    @Setter
    private String nama;

    @Column(name = "keterangan", length = 100)
    @Getter
    @Setter
    private String keterangan;

}
