package id.ac.uinjkt.eprogress.model.ep;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author Pustipanda
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(schema = "ep", name = "tugas")
public class Tugas implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, unique = true, nullable = false)
    @Setter
    @Getter
    private Long id;

    @Setter
    @Getter
    @Column(name = "nama", length = 50)
    private String nama;

    @Setter
    @Getter
    @Column(name = "durasi", length = 10)
    private Long durasi;

    @Setter
    @Getter
    @Column(name = "deskripsi", length = 100)
    private String deskripsi;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @Fetch(FetchMode.SELECT)
    @JoinColumn(name = "stakeholder")
    @Getter
    @Setter
    private Stakeholder stakeholder;

    @CreatedBy
    @Column(name = "created_by", updatable = false)
    @Setter
    @Getter
    private String createdBy;

    @CreatedDate
    @Column(name = "created", updatable = false)
    @Setter
    @Getter
    private LocalDateTime created;

    @LastModifiedBy
    @Column(name = "modified_by")
    @Setter
    @Getter
    private String modifiedBy;

    @LastModifiedDate
    @Column(name = "modified")
    @Setter
    @Getter
    private LocalDateTime modified;

}
