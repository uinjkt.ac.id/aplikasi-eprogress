package id.ac.uinjkt.eprogress.model.sso;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;


/**
 *
 * @author Pustipanda
 */
@Entity
@Table(schema = "sso", name = "s_permission")
public class Permission implements Serializable {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(name = "id", length = 36)
    @Getter
    @Setter
    private String id;

    @NotNull
    @Column(name = "permission_label", nullable = false, unique = true, length = 100)
    @Getter
    @Setter
    private String label;

    @NotNull
    @Column(name = "permission_value", nullable = false, unique = true, length = 50)
    @Getter
    @Setter
    private String value;
}
