package id.ac.uinjkt.eprogress.model.sso;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author Pustipanda
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(schema = "sso", name = "s_role")
public class Role implements Serializable {
    
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(name = "id", length = 36)
    @Getter
    @Setter
    @Id
    private String id;

    @NotNull
    @NotEmpty(message = "Name tidak boleh kosong")
    @Column(name = "name", nullable = false, unique = true, length = 30)
    @Getter
    @Setter
    private String name;

    @NotNull
    @Column(name = "description", length = 50)
    @Getter
    @Setter
    private String description;

    @ManyToMany(fetch = FetchType.EAGER)
    @OrderBy("value asc")
    @JoinTable(schema = "sso",
            name = "s_role_permission",
            joinColumns = @JoinColumn(name = "id_role", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "id_permission", nullable = false)
    )
    private Set<Permission> permissions = new HashSet<Permission>();

    @CreatedBy
    @Column(name = "created_by", updatable = false)
    @Setter
    @Getter
    private String createdBy;

    @CreatedDate
    @Column(name = "created", updatable = false)
    @Setter
    @Getter
    private LocalDateTime created;

    @LastModifiedBy
    @Column(name = "modified_by")
    @Setter
    @Getter
    private String modifiedBy;

    @LastModifiedDate
    @Column(name = "modified")
    @Setter
    @Getter
    private LocalDateTime modified;

    public Set<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<Permission> permissions) {
        this.permissions = permissions;
    }
}
