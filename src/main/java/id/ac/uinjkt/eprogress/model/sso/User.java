package id.ac.uinjkt.eprogress.model.sso;

import id.ac.uinjkt.eprogress.model.ep.Petugas;
import id.ac.uinjkt.eprogress.model.ep.Stakeholder;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author Pustipanda
 */
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(schema = "sso", name = "s_user")
public class User implements Serializable {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @Column(name = "id", length = 36)
    @Getter
    @Setter
    private String id;

    @NotEmpty(message = "Fullname tidak boleh kosong")
    @Column(name = "fullname", length = 50)
    @Getter
    @Setter
    private String fullname;

    @NotNull
    @Column(name = "username", unique = true, length = 30)
    @Getter
    @Setter
    private String username;

    @Column(name = "status")
    @Getter
    @Setter
    private Boolean status = Boolean.TRUE;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_role", nullable = false)
    @Setter
    @Getter
    private Role role;

    @NotEmpty(message = "Password tidak boleh kosong")
    @Getter
    @Setter
    @Column(name = "password", length = 100)
    private String password;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @Fetch(FetchMode.SELECT)
    @JoinColumn(name = "stakeholder")
    @Getter
    @Setter
    private Stakeholder stakeholder;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @Fetch(FetchMode.SELECT)
    @JoinColumn(name = "petugas")
    @Getter
    @Setter
    private Petugas petugas;

    @CreatedBy
    @Column(name = "created_by", updatable = false)
    @Setter
    @Getter
    private String createdBy;

    @CreatedDate
    @Column(name = "created", updatable = false)
    @Setter
    @Getter
    private LocalDateTime created;

    @LastModifiedBy
    @Column(name = "modified_by")
    @Setter
    @Getter
    private String modifiedBy;

    @LastModifiedDate
    @Column(name = "modified")
    @Setter
    @Getter
    private LocalDateTime modified;

}
