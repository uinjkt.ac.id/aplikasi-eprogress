package id.ac.uinjkt.eprogress.service;

import id.ac.uinjkt.eprogress.dao.ep.DetilTugasKegiatanDao;
import id.ac.uinjkt.eprogress.dao.ep.PetugasDao;
import id.ac.uinjkt.eprogress.dao.ep.TugasKegiatanDao;
import id.ac.uinjkt.eprogress.model.ep.DetilTugasKegiatan;
import id.ac.uinjkt.eprogress.model.ep.Petugas;
import id.ac.uinjkt.eprogress.model.ep.TugasKegiatan;
import id.ac.uinjkt.eprogress.model.sso.User;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

/**
 *
 * @author Pustipanda
 */
@Service
public class CommonService {

    @Autowired
    private DetilTugasKegiatanDao detilTugasKegiatanDao;

    @Autowired
    private TugasKegiatanDao tkd;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private PetugasDao petugasDao;

    public Date convertToDateViaSqlTimestamp(LocalDateTime dateToConvert) {
        return java.sql.Timestamp.valueOf(dateToConvert);
    }

    public void refreshProgres(Long idTugasKegiatan) {

        float progres;
        long jumlahDetilKegiatan = detilTugasKegiatanDao.countByTugasKegiatanId(idTugasKegiatan);
        long jumlahDetilKegiatanTrue = detilTugasKegiatanDao.countByTugasKegiatanIdAndStatusTrue(idTugasKegiatan);
        float maxPersen = 100;

        progres = (maxPersen / jumlahDetilKegiatan) * jumlahDetilKegiatanTrue;
        TugasKegiatan tk = tkd.findByIdOrderById(idTugasKegiatan);
        tk.setProgres((long) progres);

        tkd.save(tk);
    }

    public void updateLog(Long idTugasKegiatan, Long idDetilTugasKegiatan, String jenisPerubahan, Authentication auth) {

        User u = currentUserService.currentUser(auth);
        Optional<Petugas> petugas = petugasDao.findById(u.getPetugas().getId());

        Optional<TugasKegiatan> tugasKegiatan1 = tkd.findById(idTugasKegiatan);
        DetilTugasKegiatan detilTugasKegiatan = detilTugasKegiatanDao.findByIdOrderByIdAsc(idDetilTugasKegiatan);

        String personUpdate = "=== terakhir di" + jenisPerubahan + " oleh " + petugas.get().getDivisi() + " pada " + convertToDateViaSqlTimestamp(tugasKegiatan1.get().getModified()) + " ===";
        String perubahan = (tugasKegiatan1.get().getRincian() == null ? "" : tugasKegiatan1.get().getRincian()) + "\n" + "Menyelesaikan: " + detilTugasKegiatan.getDaftarTugasKegiatan();

        String gabunganUpdate = perubahan + "\n" + personUpdate + "\n";
        tugasKegiatan1.get().setRincian(gabunganUpdate);

        tkd.save(tugasKegiatan1.get());
    }

    public Integer showProgressPercent(Long idTugas) {

        List<TugasKegiatan> tg = tkd.findByTugasId(idTugas);

        int i = 0;
        float progres = 0;
        float tempProgres;
        long jumlahKegiatan = tkd.countByTugasId(idTugas);
        float maxPersen = 100;

        for (TugasKegiatan tgs : tg) {
            tgs.setProgres(tg.get(i).getProgres());
            tempProgres = tgs.getProgres() * ((maxPersen / jumlahKegiatan) / maxPersen);
            progres += tempProgres;
            i++;
        }

        int hasil = (int) progres;

        return hasil;
    }

}
