package id.ac.uinjkt.eprogress.service;


import id.ac.uinjkt.eprogress.dao.sso.UserDao;
import id.ac.uinjkt.eprogress.model.sso.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

/**
 *
 * @author Pustipanda
 */
@Service
public class CurrentUserService {

    private static final Logger log = LoggerFactory.getLogger(CurrentUserService.class);

    @Autowired
    private UserDao userDao;

    public User currentUser(Authentication auth) {
        String username = auth.getName();
        User u = userDao.findByUsername(username);
        //log.info("Info User -> username {} role {}", u.getUsername(), u.getRole().getName());

        return u;
    }
}
