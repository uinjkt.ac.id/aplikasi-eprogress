    create schema if not exists sso;
    create schema if not exists ep;

    create table ep.kegiatan (
       id  bigserial not null,
        deskripsi varchar(100),
        nama varchar(50),
        primary key (id)
    );
    
    create table ep.penugasan (
       id  bigserial not null,
        created timestamp,
        created_by varchar(255),
        modified timestamp,
        modified_by varchar(255),
        petugas int8,
        tugas int8,
        primary key (id)
    );
    
    create table ep.petugas (
       id  bigserial not null,
        jenis_kelamin varchar(50),
        nama varchar(50),
        nomor_absen varchar(50),
        divisi varchar(50),
        foto varchar(50),
        primary key (id)
    );
    
    create table ep.stakeholder (
       id  bigserial not null,
        keterangan varchar(100),
        nama varchar(50),
        primary key (id)
    );
    
    create table ep.tugas (
       id  bigserial not null,
        created timestamp,
        created_by varchar(255),
        deskripsi varchar(100),
        durasi int8,
        modified timestamp,
        modified_by varchar(255),
        nama varchar(50),
        stakeholder int8,
        primary key (id)
    );
    
    create table ep.tugas_kegiatan (
       id  bigserial not null,
        created timestamp,
        created_by varchar(255),
        modified timestamp,
        modified_by varchar(255),
        progres int8,
        rincian text,
        status bool,
        kegiatan int8,
        tugas int8,
        primary key (id)
    );
    
    create table ep.detil_tugas_kegiatan (
       id  bigserial not null,
        created timestamp,
        created_by varchar(255),
        daftar_tugas_kegiatan varchar(255),
        modified timestamp,
        modified_by varchar(255),
        status boolean,
        tugas_kegiatan int8,
        primary key (id)
    );

    create table sso.s_permission (
       id varchar(36) not null,
        permission_label varchar(100) not null,
        permission_value varchar(50) not null,
        primary key (id)
    );
    
    create table sso.s_role (
       id varchar(36) not null,
        created timestamp,
        created_by varchar(255),
        description varchar(50) not null,
        modified timestamp,
        modified_by varchar(255),
        name varchar(30) not null,
        primary key (id)
    );
    
    create table sso.s_role_permission (
       id_role varchar(36) not null,
        id_permission varchar(36) not null,
        primary key (id_role, id_permission)
    );
    
    create table sso.s_user (
       id varchar(36) not null,
        created timestamp,
        created_by varchar(255),
        fullname varchar(50),
        modified timestamp,
        modified_by varchar(255),
        password varchar(100),
        status boolean,
        username varchar(30) not null,
        petugas int8,
        id_role varchar(36) not null,
        stakeholder int8,
        primary key (id)
    );
    
    alter table if exists sso.s_permission 
       drop constraint if exists UK_r7lpxanjknx4vvsklc9kean6k;
    
    alter table if exists sso.s_permission 
       add constraint UK_r7lpxanjknx4vvsklc9kean6k unique (permission_label);
    
    alter table if exists sso.s_permission 
       drop constraint if exists UK_80k9msnfcruttu7y53i2uea9w;
    
    alter table if exists sso.s_permission 
       add constraint UK_80k9msnfcruttu7y53i2uea9w unique (permission_value);

    alter table if exists sso.s_role 
       drop constraint if exists UK_loaswm4x6syq1yhm659kmkhse;
    
    alter table if exists sso.s_role 
       add constraint UK_loaswm4x6syq1yhm659kmkhse unique (name);

    alter table if exists sso.s_user 
       drop constraint if exists UK_ph4j8u4d8et9etejdr9mig3mr;
    
    alter table if exists sso.s_user 
       add constraint UK_ph4j8u4d8et9etejdr9mig3mr unique (username);
    
    alter table if exists ep.penugasan 
       add constraint FKjv0w87e8x7r91rux3nag92vkc 
       foreign key (petugas) 
       references ep.petugas;
    
    alter table if exists ep.penugasan 
       add constraint FK6v7x01rarivcb0hc526vm6j5d 
       foreign key (tugas) 
       references ep.tugas;
    
    alter table if exists ep.tugas 
       add constraint FKm8lua3jogojt4kimrympvkfix 
       foreign key (stakeholder) 
       references ep.stakeholder;
    
    alter table if exists ep.tugas_kegiatan 
       add constraint FKknla5ytcme62vofq19bhsqy24 
       foreign key (kegiatan) 
       references ep.kegiatan;
    
    alter table if exists ep.tugas_kegiatan 
       add constraint FKflumggmri8g4cmfqpy2u0aehu 
       foreign key (tugas) 
       references ep.tugas;
    
    alter table if exists ep.detil_tugas_kegiatan 
       add constraint FK1rpspgllbpwcyb15ercgfraw 
       foreign key (tugas_kegiatan) 
       references ep.tugas_kegiatan;

    alter table if exists sso.s_role_permission 
       add constraint FKctdvc3x67tpy90xh905iiautx 
       foreign key (id_permission) 
       references sso.s_permission;
    
    alter table if exists sso.s_role_permission 
       add constraint FK9cj7fdg3hf41td1n0vebmfa5x 
       foreign key (id_role) 
       references sso.s_role;
    
    alter table if exists sso.s_user 
       add constraint FK56yprgfwhq93u0ip7d7pbxbhl 
       foreign key (petugas) 
       references ep.petugas;
    
    alter table if exists sso.s_user 
       add constraint FK9g0pu3y0px1480kjlac10qd80 
       foreign key (id_role) 
       references sso.s_role;
    
    alter table if exists sso.s_user 
       add constraint FKrfs79h0u40gt5i4f83ew3wdd0 
       foreign key (stakeholder) 
       references ep.stakeholder;