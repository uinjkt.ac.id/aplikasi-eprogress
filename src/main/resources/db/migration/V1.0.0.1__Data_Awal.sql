INSERT INTO sso.s_permission(id, permission_label, permission_value) VALUES
('LOGGED_IN', 'Get User Loggin Information', 'ROLE_LOGGED_IN');

INSERT INTO sso.s_role (id, description, name) VALUES
('AM', 'Role Administrator', 'Administrator'),
('STK', 'Role Stakehoder', 'Stakeholder'),
('PG', 'Role Petugas', 'Petugas');

INSERT INTO sso.s_role_permission (id_role, id_permission) VALUES
('AM', 'LOGGED_IN'),
('STK', 'LOGGED_IN'),
('PG', 'LOGGED_IN');

INSERT INTO sso.s_user (id, status, username, password, fullname, id_role) VALUES
('root', true, 'root', '$2a$10$zbKwuKOubxLBjFZnQrWHlOYR5ji479.DCkEtDXAvoevYWFhoYpuDS', 'Administrator', 'AM');